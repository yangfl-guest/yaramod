#!/bin/sh
set -e

mkdir build
cd build
cmake .. \
  -DYARAMOD_LIB=OFF -DYARAMOD_VERSION=3.5.0 -DYARAMOD_TESTS=ON -DCMAKE_STRIP=OFF \
  -DYARAMOD_PYTHON=ON -DPYTHON_EXECUTABLE=/usr/bin/python3
make yaramod_tests
./tests/cpp/yaramod_tests
cd ..
rm -rf build
